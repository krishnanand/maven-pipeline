package com.example.mavenpipeline.reader;

import com.google.gson.Gson;

import java.io.InputStream;
import java.io.InputStreamReader;

public class JsonReader {

    public <T> T map(String fileName, Class<T> tClass) {
        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream(fileName);
        Gson gson = new Gson();
        return gson.fromJson(new InputStreamReader(resourceAsStream), tClass);
    }
}
