package com.example.mavenpipeline.tests;

import com.example.mavenpipeline.models.Sample;
import com.example.mavenpipeline.reader.JsonReader;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;

@Epic("Sample EPIC")
@Feature("Sample Feature")
public class SampleTest {

    @Test
    @Story("Sample Story")
    @Description("Sample Description")
    public void sampleTest() {
        JsonReader jsonReader = new JsonReader();
        Sample sample = jsonReader.map("sample.json", Sample.class);
        Assert.assertEquals(sample.getName(), "hello");
    }
}
